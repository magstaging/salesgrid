# README #

Create 2 columns dynamically in sales order grid backend screen. It takes the order created at value
and split it into 2: day and time. Each new column renders day and the other the time when the order was created.

Of course, this is a simple/dummy implementation; in real life, the dynamic columns may take their value from more useful classes

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/SalesGrid when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

