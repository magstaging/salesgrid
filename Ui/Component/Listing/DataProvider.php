<?php

namespace Mbs\SalesGrid\Ui\Component\Listing;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\ReportingInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;

class DataProvider extends \Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider
{
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        ReportingInterface $reporting,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        RequestInterface $request,
        FilterBuilder $filterBuilder,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $reporting, $searchCriteriaBuilder, $request, $filterBuilder, $meta, $data);
    }

    public function getData()
    {
        $data = parent::getData();

        foreach ($data['items'] as &$item) {
            $item['sales_day_created'] = $this->extraDayFromDate($item['created_at']);
            $item['sales_time_created'] = $this->extraTimeFromDate($item['created_at']);
        }

        return $data;
    }

    private function extraDayFromDate($createdAt)
    {
        $date = new \DateTimeImmutable($createdAt);
        return $date->format('D');
    }

    private function extraTimeFromDate($createdAt)
    {
        $date = new \DateTimeImmutable($createdAt);
        return $date->format('H:i:s');
    }
}
