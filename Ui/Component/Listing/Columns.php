<?php

namespace Mbs\SalesGrid\Ui\Component\Listing;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Mbs\SalesGrid\Ui\Component\Listing\Column\CustomColumnCreator;

class Columns extends \Magento\Ui\Component\Listing\Columns
{
    /**
     * @var CustomColumnCreator
     */
    private $columnCreator;

    public function __construct(
        ContextInterface $context,
        CustomColumnCreator $columnCreator,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $components, $data);
        $this->columnCreator = $columnCreator;
    }

    public function prepare()
    {
        if (isset($this->components['created_at'])) {
            $billingNameColumn = $this->components['billing_name'];

            $column = $this->columnCreator->addColumnFromExistingColumn(
                $billingNameColumn,
                'sales_day_created',
                'Sales Day',
                11
            );
            $this->addComponent('sales_day_created', $column);

            $column = $this->columnCreator->addColumnFromExistingColumn(
                $billingNameColumn,
                'sales_time_created',
                'Sales Time',
                11
            );
            $this->addComponent('sales_time_created', $column);
        }

        parent::prepare();
    }


}
